import React,{useEffect, useState} from 'react';
import Banner from "./components/Banner"
import Header from './components/Header';
import NavMobile from './components/NavMobile';
import Experience from "./components/Experience"
import Video from './components/Video';
import Headsets from "./components/Headsets"
import Testimonial from "./components/Testimonial"
import Explore from "./components/Explore"
import "aos/dist/aos.css";
import Aos from "aos"
const App = () => {
  const [navMobile,setNavMobile]=useState(false)
  useEffect(()=>{
    Aos.init({
      duration:2500,
      delay:400
    })
  },[])
  return (
    <div>
      <Header setNavMobile={setNavMobile}/>
      <Banner/>
      {
        <div className={`${navMobile?"right-0":"-right-full"} fixed top-0 bottom-0 w-48 transition-all`}>
            <NavMobile setNavMobile={setNavMobile}/>
        </div>
      }
      <Experience/>
      <Video/>
      <Headsets />
      <Testimonial />
      <Explore/>
    </div>
  )
};

export default App;
