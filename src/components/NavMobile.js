import React from 'react';
import {IoClose} from "react-icons/io5"
const NavMobile = ({setNavMobile}) => {
  return <nav className='lg:hidden bg-[#251f3f] w-full h-full'>
      <IoClose className="text-3xl absolute left-4 top-6 cursor-pointer" onClick={()=>setNavMobile(false)}/>
      <ul className='flex flex-col items-center pt-20 space-y-12'>
          <li className='text-lg'><a href='/'>Home</a></li>
          <li className='text-lg'><a href='/'>Company</a></li>
          <li className='text-lg'><a href='/'>Features</a></li>
          <li className='btn'><a href='/'>Sign up</a></li>
      </ul>
  </nav>;
};

export default NavMobile;
