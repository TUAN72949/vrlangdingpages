import React from 'react';
import Logo from "../assets/img/logo.svg"
import Nav from "./Nav"
import {HiMenu} from "react-icons/hi"
const Header = ({setNavMobile}) => {
  return <header className='py-6' data-aos="fade-down" data-aos-duration="2000" data-aos-delay="900">
        <div className='container mx-auto'>
          <div className='flex justify-between items-center'>
            <a href='/'>
              <img className='h-[30px]' src={Logo} alt=""/>
            </a>
            <Nav/>
            <HiMenu className="text-white text-3xl lg:hidden cursor-pointer" onClick={()=>setNavMobile(true)}/>
          </div>
        </div>
  </header>;
};

export default Header;
