import React from 'react';
// import Users from "./Users";
import Img from "../assets/img/banner-img.png" 
import Avatar1 from "../assets/img/avt1.png"
import Avatar2 from "../assets/img/avt2.png"
import Avatar3 from "../assets/img/avt3.png"
import Avatar4 from "../assets/img/avt4.png"
import {BsFillCircleFill} from "react-icons/bs"
const Banner = () => {
  return <section className='min-h-[600px] pt-24 pb-12 text-center relative lg:pt-48 lg:pb-0 lg:text-left'>
    <div className='container mx-auto'>
        <div className='flex flex-col lg:flex-row'>
          <div>
            <h1 className='text-3xl fond-bold mb-8 lg:text-5xl lg:leading-snug'
            data-aos="fade-down" data-aos-delay="500"
            >Let's Explore<br/>Three-Dimensional Visually</h1>
            <p data-aos="fade-down" data-aos-delay="600" className='font-secondary mb-12 max-w-[440px] mx-auto lg:mx-0'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
              nisi ut aliquip ex ea commodo consequat.
            </p>
            <div data-aos="fade-down" data-aos-delay="700" className='flex items-center justify-around space-x-4 max-w-[320px] mx-auto mb-12 lg:mx-0 lg:-ml-3'>
              <button className='btn'>Get it now</button>
              <a href='#/' className='border-b-2 border-transparent hover:border-white transition-all ease-linear duration-200'>Explore Device</a>
            </div>
            <div className='flex flex-col justify-center items-center 
              space-x-5 space-y-2 lg:flex-row lg:space-y-0 lg:justify-start'
              data-aos="fade-down" data-aos-delay="900"
              >
                <div className='flex -space-x-2'>
                  <div className='w-12 h-12 rounded-full'>
                    <img src={Avatar1} alt=""/>
                  </div>
                  <div className='w-12 h-12 rounded-full'>
                    <img src={Avatar2} alt=""/>
                  </div>
                  <div className='w-12 h-12 rounded-full'>
                    <img src={Avatar3} alt=""/>
                  </div>
                  <div className='w-12 h-12 rounded-full'>
                    <img src={Avatar4} alt=""/>
                  </div>
                </div>
                <div className='flex items-center space-x-2'>
                  <BsFillCircleFill className="text-xs text-green-500 animate-pulse"/>
                  <span className='text-base font-medium'>400k people online</span>  
            </div>
   
    </div>
          </div>
          <div data-aos="fade-up" data-aos-delay="800">
            <img src={Img} alt=""/>
          </div>
        </div>
    </div>
  </section>;
};

export default Banner;
